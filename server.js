const express = require("express");
const app = express();
const port = require("./api/config/properties").port;

const bodyParser = require('body-parser');
const auth = require('./api/routes/auth/verify-token');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/auth', require('./api/routes/auth'));
app.use('/users', auth, require('./api/routes/user-routes'));
app.use('/signup', require('./api/routes/register.routes'));
app.listen(port, () => {
    console.log("Server listening on port " + port);
});