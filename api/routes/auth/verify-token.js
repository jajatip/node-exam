var jwt = require('jsonwebtoken');
var config = require('../../config/properties');
const models = require("../../models/user-model");

function verifyToken(req, res, next) {
  var token = req.headers['x-access-token'];
  if (!token) {
    return res.status(403).send({ auth: false, message: 'No token provided.' });
  }
  jwt.verify(token, config.secret, function (err, decoded) {
    if (err) {
      return res.status(403).send({ auth: false, message: 'Failed to authenticate token.' });
    }
    req.userId = decoded.id;
    models.findOne({ _id: req.userId })
      .then((user) => {
        if (!user) {
          return res.status(403).send({ auth: false, message: 'Token is not valid. Please provide valid token.' });
        }
        req.curentUser = user;
        next();
      })
      .catch((error) => {
        return res.status(500).send({ auth: false, message: 'Internal Server error.' });
      });
  });
}

module.exports = verifyToken;