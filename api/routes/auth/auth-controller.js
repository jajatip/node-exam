const express = require('express');
const router = express.Router();
const models = require("../../models/user-model");
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const config = require('../../config/properties');

router.post('/login', function (req, res) {
  models.findOne({ email: req.body.email }).select('+password')
    .then((user) => {
      if (!user)
        return res.status(401).send('No user found.');
      let passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
      if (!passwordIsValid)
        return res.status(404).send('Please enter valid password.');
      let token = jwt.sign({ id: user.id }, config.secret, {
        expiresIn: config.expires
      });
      user.password = '';
      let responseUser = { auth: true, token: token, user: user };
      return res.status(200).send(responseUser);
    })
    .catch((error) => {
      return res.status(500).send('Internal server error!.');;
    });
});

module.exports = router;