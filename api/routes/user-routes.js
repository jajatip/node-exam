const express = require('express');
const router = express.Router();
const userControllr = require('../controllers/user_controller');
const adminAuth = require('./auth/auth-user');

router.get('/', adminAuth, userControllr.findAll);
router.post('/', adminAuth, userControllr.create);
router.get('/:id',adminAuth, userControllr.findOne);
router.put('/:id', adminAuth, userControllr.update);
router.delete('/:id', adminAuth, userControllr.delete);

module.exports = router;