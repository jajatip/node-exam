const User = require('../models/user-model');
const bcrypt = require('bcryptjs');

// Create  a new user
exports.create = (req, res) => {
    // Validate request
    if(!req.body.email || !req.body.password || !req.body.name ) {
        return res.status(400).send({
            message: "Required field can not be empty"
        });
    }

    // Create a User
    const user = new User({
        email: req.body.email, 
        password: bcrypt.hashSync(req.body.password, 10),
        name: req.body.name, 
        age: req.body.age,
        isActive: req.body.isActive,
        roles: req.body.roles
    });

    // Save User in the database
    user.save().then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the User."
        });
    });
};

// Retrieve all users from the database.
exports.findAll = (req, res) => {
    User.find()
    .then(users => {
        res.status(200).send(users);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving users."
        });
    });
};

// Find a single user with a userId
exports.findOne = (req, res) => {
    User.findById(req.params.id)
    .then(user => {
        if(!user) {
            return res.status(404).send({
                message: "User not found with id " + req.params.id
            });            
        }
        res.status(200).send(user);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "User not found with id " + req.params.id
            });                
        }
        return res.status(500).send({
            message: "Error retrieving user with id " + req.params.id
        });
    });
};

// Update a user identified by the userId in the request
exports.update = (req, res, next) => {
    if(!req.body.email || !req.body.password || !req.body.name ) {
        return res.status(400).send({
            message: "Required field can not be empty"
        });
    }
    User.findByIdAndUpdate(req.params.id, req.body, {new: true})
.then(user => {
    if(!user) {
        return res.status(404).send({
            message: "User not found with id " + req.params.id
        });
    }
    res.send(user);
}).catch(err => {
    if(err.kind === 'ObjectId') {
        return res.status(404).send({
            message: "User not found with id " + req.params.id
        });                
    }
    return res.status(500).send({
        message: "Error updating user with id " + req.params.id
    });
});
};

// Delete a user with the specified id in the request
exports.delete = (req, res) => {
    User.findByIdAndRemove(req.params.id)
    .then(user => {
        if(!user) {
            return res.status(404).send({
                message: "User not found with id " + req.params.id
            });
        }
        res.send({message: "User deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "User not found with id " + req.params.id
            });                
        }
        return res.status(500).send({
            message: "Could not delete user with id " + req.params.id
        });
    });
};

exports.login = async function authenticate({ username, password }) {
    const user = User.find(u => u.username === username && u.password === password);
    if (user) {
        const token = jwt.sign({ sub: user.id, role: user.role }, config.secret);
        const { password, ...userWithoutPassword } = user;
        return {
            ...userWithoutPassword,
            token
        };
    }
}