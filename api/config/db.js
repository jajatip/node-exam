const dbURL = require('./properties').db;

const mongoose = require("mongoose");

mongoose.Promise = Promise;

mongoose.connect(dbURL, { useNewUrlParser: true })
  .then(() =>  console.log('connection succesful'))
  .catch((err) => console.error(err));

module.exports = mongoose;

