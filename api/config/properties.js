module.exports = {
    port : 4000,
    db : 'mongodb://localhost:27017/poc-db',
    secret: 'supersecret',
    expires: 86400, // expires in 24 hrs
}