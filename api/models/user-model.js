const mongoose = require("../config/db");
const schema = new mongoose.Schema({
    email: {
        desc: "The user's email address.",
        trim: true,
        type: String,
        index: true, 
        unique: true, 
        required: true
    },
    password: {
        desc: "user password",
        trim: true,
        type: String,
        required: true,
        select: false
    },   
    name: {
        desc: "The user's name.",
        trim: true,
        type: String,
        required: true
    },
    age: {
        desc: "The users's age.",
        type: Number
    },
    isActive: {
        desc: "is Active.",
        type: Boolean,
        default: true,
        required: true
    },
    roles: {
        desc: "user rloes.",
        trim: true,
        type: String,
        enum : ['Admin','User'],
        default: 'Admin',
        required: true
    },
}, {
    timestamps: true
});

module.exports = mongoose.model('Users', schema);
